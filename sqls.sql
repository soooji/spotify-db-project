

CREATE TABLE `admin` (
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;


CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `genre` varchar(200) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;


INSERT INTO `album` (`id`, `artist_id`, `name`, `genre`) VALUES
(16, 3, 'SampleAlbum', 'horror');


CREATE TABLE `album_song` (
  `album_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `addition_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `album_song` (`album_id`, `song_id`, `addition_date`) VALUES
(16, 32, '2020-07-17');


CREATE TABLE `artist` (
  `user_id` int(11) NOT NULL,
  `nationality` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `start_date` date NOT NULL,
  `artistic_name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;


INSERT INTO `artist` (`user_id`, `nationality`, `start_date`, `artistic_name`, `is_active`) VALUES
(3, 'iran', '1999-05-06', 'sooji', 0);



CREATE TABLE `buy` (
  `user_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `buy_date` date NOT NULL,
  `end_date` date NOT NULL,
  `card_number` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `card_expiration_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



CREATE TABLE `client` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `last_name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `nationality` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `birth_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `client` (`user_id`, `first_name`, `last_name`, `nationality`, `birth_date`) VALUES
(2, 'sajadi', 'beheshti', 'iran', '1999-05-06');



CREATE TABLE `follow` (
  `follower_id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `active_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



CREATE TABLE `play` (
  `song_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `play_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



CREATE TABLE `playlist` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `last_update` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `playlist` (`id`, `name`, `last_update`) VALUES
(1, 'Todays Top Hits', '2020-07-17');


CREATE TABLE `playlist_like` (
  `user_id` int(11) NOT NULL,
  `playlist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



CREATE TABLE `playlist_manage` (
  `user_id` int(11) NOT NULL,
  `playlist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `playlist_manage` (`user_id`, `playlist_id`) VALUES
(1, 1),
(3, 1);



CREATE TABLE `playlist_song` (
  `playlist_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `addition_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



CREATE TABLE `publish` (
  `artist_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `publish_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `publish` (`artist_id`, `album_id`, `publish_date`) VALUES
(3, 16, '2020-07-17');



CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` varchar(200) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `report` (`id`, `song_id`, `user_id`, `description`) VALUES
(1, 32, 3, 'I don\'t like this song, so i report it :|');



CREATE TABLE `song` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `song` (`id`, `name`, `duration`) VALUES
(32, 'TwoDir', 190);



CREATE TABLE `song_like` (
  `user_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `song_like` (`user_id`, `song_id`) VALUES
(3, 32);



CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `salt` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `security_question` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `security_answer` varchar(200) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



INSERT INTO `users` (`id`, `username`, `email`, `password`, `salt`, `security_question`, `security_answer`) VALUES
(1, 'sooji', 'beheshtisajad@gmail.com', '8e03c8b9970867b268d7d392a1a9378b020bbd6165d101cacb41ccdd6dfd943f57a6ef7ce6cd760cf1e2f939323b888b87a4163e4aa08365666eeb90247470de', '92c027ccb7c66b80', 'What\'s your fum favorite teacher?', 'nobody'),
(2, 'soojibht', 'soojibht@gmail.com', '41387bd7d465afd9bafb528d5d416e27df0fd200be05a3c59e6fa17813d9a0793590004d7f8a0d04ccc0f2d5e904f9bb92a3c6b59072b802a454a445cbd12247', '0c72f26ef3155d90', 'What\'s your fum favorite teacher?', 'nobody'),
(3, 'sjd', 'a@m.com', 'babfc48152ceae4a4e88f22fd3254317a59b84473948a6a50c5a3aacc7b651fa844c851a84abe6186c64c854eb0d90d9b6c4e9150675930a71530de145e79203', '643a9ce16a2121fc', 'What\'s your fum favorite teacher?', 'nobody');


ALTER TABLE `admin`
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `album`
  ADD PRIMARY KEY (`id`),
  ADD KEY `artist_id` (`artist_id`);


ALTER TABLE `album_song`
  ADD PRIMARY KEY (`album_id`,`song_id`),
  ADD KEY `song_id` (`song_id`);


ALTER TABLE `artist`
  ADD PRIMARY KEY (`user_id`);


ALTER TABLE `buy`
  ADD PRIMARY KEY (`user_id`,`plan_id`),
  ADD KEY `plan_id` (`plan_id`);


ALTER TABLE `client`
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `follow`
  ADD PRIMARY KEY (`follower_id`,`following_id`),
  ADD KEY `following_id` (`following_id`);


ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `play`
  ADD PRIMARY KEY (`song_id`,`user_id`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `playlist`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `playlist_like`
  ADD PRIMARY KEY (`user_id`,`playlist_id`),
  ADD KEY `playlist_id` (`playlist_id`);


ALTER TABLE `playlist_manage`
  ADD PRIMARY KEY (`user_id`,`playlist_id`),
  ADD KEY `playlist_id` (`playlist_id`);


ALTER TABLE `playlist_song`
  ADD PRIMARY KEY (`playlist_id`,`song_id`),
  ADD KEY `song_id` (`song_id`);


ALTER TABLE `publish`
  ADD PRIMARY KEY (`artist_id`,`album_id`),
  ADD KEY `album_id` (`album_id`);


ALTER TABLE `report`
  ADD PRIMARY KEY (`id`,`song_id`,`user_id`),
  ADD KEY `song_id` (`song_id`),
  ADD KEY `user_id` (`user_id`);


ALTER TABLE `song`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `song_like`
  ADD PRIMARY KEY (`user_id`,`song_id`),
  ADD KEY `song_id` (`song_id`);


ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);


ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;


ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `playlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;


ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;


ALTER TABLE `song`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;


ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);


ALTER TABLE `album`
  ADD CONSTRAINT `album_ibfk_1` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`user_id`) ON DELETE CASCADE;


ALTER TABLE `album_song`
  ADD CONSTRAINT `album_song_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `album_song_ibfk_2` FOREIGN KEY (`song_id`) REFERENCES `song` (`id`) ON DELETE CASCADE;


ALTER TABLE `artist`
  ADD CONSTRAINT `artist_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;


ALTER TABLE `buy`
  ADD CONSTRAINT `buy_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `buy_ibfk_2` FOREIGN KEY (`plan_id`) REFERENCES `plan` (`id`) ON DELETE CASCADE;


ALTER TABLE `client`
  ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;


ALTER TABLE `follow`
  ADD CONSTRAINT `follow_ibfk_1` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `follow_ibfk_2` FOREIGN KEY (`following_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;


ALTER TABLE `play`
  ADD CONSTRAINT `play_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `play_ibfk_2` FOREIGN KEY (`song_id`) REFERENCES `song` (`id`) ON DELETE CASCADE;


ALTER TABLE `playlist_like`
  ADD CONSTRAINT `playlist_like_ibfk_1` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `playlist_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;


ALTER TABLE `playlist_manage`
  ADD CONSTRAINT `playlist_manage_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `playlist_manage_ibfk_2` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`) ON DELETE CASCADE;


ALTER TABLE `playlist_song`
  ADD CONSTRAINT `playlist_song_ibfk_1` FOREIGN KEY (`playlist_id`) REFERENCES `playlist` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `playlist_song_ibfk_2` FOREIGN KEY (`song_id`) REFERENCES `song` (`id`) ON DELETE CASCADE;


ALTER TABLE `publish`
  ADD CONSTRAINT `publish_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `publish_ibfk_2` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`user_id`) ON DELETE CASCADE;


ALTER TABLE `report`
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`song_id`) REFERENCES `song` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `report_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;


ALTER TABLE `song_like`
  ADD CONSTRAINT `song_like_ibfk_1` FOREIGN KEY (`song_id`) REFERENCES `song` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `song_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

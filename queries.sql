-- سرچ های اصلی:
-- :: جستجو در کاربران
SELECT id,username
from users
WHERE username LIKE "%a%"

-- :: جستجو در موسیقی‌ها
SELECT song_id,song_name,artist_id,artistic_name
FROM(SELECT publish.artist_id,f2.song_id,f2.song_name
    FROM (
        SELECT f1.name AS song_name,f1.id AS song_id,album_song.album_id
        FROM (
                SELECT song.id,song.name
                from song
                WHERE name LIKE "%1%") f1
        JOIN album_song ON album_song.song_id = f1.id) f2
    JOIN publish ON publish.album_id = f2.album_id) f3
JOIN artist ON artist.user_id = f3.artist_id

-- :: جستجو در آلبوم‌ها
SELECT f2.id as album_id,f2.name as album_name,artist.user_id as artist_id, artist.artistic_name
FROM(
	SELECT f1.id, f1.name, publish.artist_id
    FROM(
        SELECT album.id,album.name
        from album
        WHERE name LIKE "%a%") f1
    JOIN publish ON publish.album_id = f1.id) f2
JOIN artist ON artist.user_id = f2.artist_id

-- :: جستجو در پلی‌لیست‌ها
SELECT *
FROM playlist
WHERE name LIKE "%a%"


-- تعداد فالورهای کاربر:
SELECT users.id as user_id, count(follower_id) as followers
FROM follow
JOIN users ON follow.following_id = users.id
WHERE users.id = 3

-- دریافت لیست فالورهای کاربر:
SELECT users.username
FROM (SELECT follower_id
FROM follow
JOIN users ON follow.following_id = users.id
WHERE users.id = 3) f1
JOIN users ON users.id = f1.follower_id

-- تعداد فالوینگ های کاربر:
SELECT users.id as user_id, count(following_id) as followings
FROM follow
JOIN users ON follow.follower_id = users.id
WHERE users.id = 3

-- دریافت لیست فالوینگ کاربر:
SELECT users.username
FROM (SELECT following_id
FROM follow
JOIN users ON follow.follower_id = users.id
WHERE users.id = 3) f1
JOIN users ON users.id = f1.following_id

-- دریافت پلی لیست های ساخته شده توسط کاربر به همراه تعداد آهنگ های هرکدام:
SELECT playlist.last_update,playlist.name,COUNT(playlist_song.song_id) as song_count
FROM ((playlist_manage
JOIN playlist ON playlist.id = playlist_manage.playlist_id)
JOIN playlist_song ON playlist_song.playlist_id = playlist.id)
WHERE playlist_manage.user_id = 1
GROUP BY playlist_song.playlist_id

-- دریافت ژانر قالب هنرمند:
SELECT genre
FROM album
JOIN publish ON album.id = publish.album_id
WHERE publish.artist_id = 3
GROUP BY album.genre
HAVING COUNT(album.id) >= ALL(SELECT COUNT(album.id)
                              	FROM album
								JOIN publish ON album.id = publish.album_id
								WHERE publish.artist_id = 3
								GROUP BY album.genre)

-- دریافت لیست آلبوم‌های یک هنرمند:
SELECT album.name,publish.publish_date
FROM album
JOIN publish ON album.id = publish.album_id
WHERE publish.artist_id = 3


-- لیست محبوب ترین آهنگ‌های هنرمند:
SELECT plays.song_id,likes.like_count + plays.play_count AS total
FROM(
	    SELECT song.id AS song_id,COUNT(song.id) AS like_count
		FROM ((((publish
		JOIN album ON publish.album_id = album.id)
	 	JOIN album_song ON album.id = album_song.album_id)
	    JOIN song ON album_song.song_id = song.id)
	    JOIN song_like ON song.id = song_like.song_id)
		WHERE publish.artist_id = 3
		GROUP BY song.id) likes
JOIN (
    	SELECT song.id AS song_id,COUNT(song.id) AS play_count
		FROM ((((publish
		JOIN album ON publish.album_id = album.id)
	 	JOIN album_song ON album.id = album_song.album_id)
	    JOIN song ON album_song.song_id = song.id)
    	JOIN play ON song.id = play.song_id)
		WHERE publish.artist_id = 3
		GROUP BY song.id) plays
ON likes.song_id = plays.song_id
ORDER BY total DESC



-- آخرین موسیقی که توسط کاربرانی که فالوکرده‌اند پخش شده:

SELECT MAXD.user_id,MAXD.username,LS1.song_id,LS1.song_name,LS1.play_date,LS1.duration
FROM(
    SELECT user_id,username, play_date,song.id AS song_id,song.name AS song_name, song.duration
    FROM(
        SELECT f2.user_id,f2.username,play.song_id, play.play_date
        FROM (
            SELECT followings.username,followings.id AS user_id
            FROM(
                SELECT users.id as user_id, following_id
                FROM follow
                JOIN users ON follow.follower_id = users.id
                WHERE users.id = 4) f1
            JOIN users AS followings ON followings.id = f1.following_id) f2
        JOIN play ON play.user_id = f2.user_id) f3
    JOIN song ON song.id = f3.song_id) LS1
INNER JOIN(
    SELECT user_id,username, MAX(play_date) AS play_date
    FROM(
        SELECT f2.user_id,f2.username,play.song_id, play.play_date
        FROM (
            SELECT followings.username,followings.id AS user_id
            FROM(
                SELECT users.id as user_id, following_id
                FROM follow
                JOIN users ON follow.follower_id = users.id
                WHERE users.id = 4) f1
            JOIN users AS followings ON followings.id = f1.following_id) f2
        JOIN play ON play.user_id = f2.user_id) f3
    JOIN song ON song.id = f3.song_id
    GROUP BY username) MAXD
    ON MAXD.user_id = LS1.user_id AND MAXD.play_date = LS1.play_date




-- ۵ موسیقی جدید هنرمندانی که کاربر دنبال می‌کنه:

SELECT song.id AS song_id,song.name AS song_name,song.duration,f5.artist_username,f5.artist_id,f5.addition_date
FROM(
    SELECT album_song.song_id, f4.artist_username,f4.artist_id, album_song.addition_date
    FROM(
        SELECT publish.album_id, f3.artist_username, f3.artist_id
        FROM(
            SELECT artist.user_id, f2.artist_username, artist.user_id AS artist_id
            FROM(
                SELECT users.username,users.id, users.username AS artist_username
                FROM (
                    SELECT following_id
                    FROM follow
                    JOIN users ON follow.follower_id = users.id
                    WHERE users.id = 4) f1
                JOIN users ON users.id = f1.following_id) f2
            JOIN artist ON artist.user_id = f2.id) f3
        JOIN publish ON f3.user_id = publish.artist_id) f4
    JOIN album_song ON album_song.album_id = f4.album_id) f5
JOIN song ON song.id = f5.song_id
ORDER BY f5.addition_date DESC
LIMIT 5
var plan = {
  active_days: {
    presence: {
      message: "is required",
    },
    type: {
      type: "number",
      message: "should be number!",
    },
  },
};

var buy = {
  card_number: {
    presence: {
      message: "is required",
    },
  },
  card_expiration_date: {
    presence: {
      message: "is required",
    },
    datetime: {
      message: "shoud be a valid date",
    }
  },
};
module.exports = { plan: plan, buy: buy };

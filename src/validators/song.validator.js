var song = {
  name: {
    presence: {
      message: "is required",
    },
  },
  duration: {
    presence: {
      message: "is required",
    },
  },
};

var report = {
  song_id: {
    presence: {
      message: "is required",
    },
  },
};
module.exports = { song: song,report: report };

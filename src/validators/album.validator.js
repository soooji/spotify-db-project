var album = {
   name: {
     presence: {
       message: "is required",
     },
   },
   genre: {
     presence: {
       message: "is required",
     },
   },
   songs: {
      presence: {
        message: "is required and should not be empty!",
        allowEmpty: false
      },
      type: {
         type: "array",
         message: "should be array!",
      }
   }
 };
 var put = {
   name: {
     presence: {
       message: "is required",
     },
   },
   genre: {
     presence: {
       message: "is required",
     },
   },
 };
 module.exports = { album: album,put: put };
 
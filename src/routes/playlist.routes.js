const express = require('express')
const router = express.Router()
const controller =   require('../controllers/playlist.controller');
const connectEnsureLogin = require('connect-ensure-login');

router.post('/', connectEnsureLogin.ensureLoggedIn(),controller.postPlaylist);
router.get('/:id', connectEnsureLogin.ensureLoggedIn(),controller.getPlaylist);
router.put('/:id', connectEnsureLogin.ensureLoggedIn(),controller.putPlaylist);
router.delete('/:id', connectEnsureLogin.ensureLoggedIn(),controller.deletePlaylist);
router.get('/:id/songs/:song_id', connectEnsureLogin.ensureLoggedIn(),controller.addSong);
router.delete('/:id/songs/:song_id', connectEnsureLogin.ensureLoggedIn(),controller.removeSong);
router.get('/:id/like', connectEnsureLogin.ensureLoggedIn(),controller.like);
router.get('/:id/addmanager/:user_id', connectEnsureLogin.ensureLoggedIn(),controller.addManager);

module.exports = router
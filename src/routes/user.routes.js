const express = require('express')
const router = express.Router()
const userController =   require('../controllers/user.controller');
const connectEnsureLogin = require('connect-ensure-login');
// Retrieve all users
router.get('/',connectEnsureLogin.ensureLoggedIn(), userController.getUser);
router.post('/client',connectEnsureLogin.ensureLoggedIn(), userController.createClientProfile);
router.post('/artist',connectEnsureLogin.ensureLoggedIn(), userController.createArtistProfile);
router.put('/client',connectEnsureLogin.ensureLoggedIn(), userController.updateClientProfile);
router.put('/artist',connectEnsureLogin.ensureLoggedIn(), userController.updateArtistProfile);
router.get('/follow/:id',connectEnsureLogin.ensureLoggedIn(), userController.follow);
module.exports = router

// Create a new uer
// router.post('/', userController.create);
// Retrieve a single user with id
// router.get('/:id', userController.findById);
// Update a user with id
// router.put('/:id', userController.update);
// Delete a user with id
// router.delete('/:id', userController.delete);
const express = require('express')
const router = express.Router()
const controller = require('../controllers/plan.controller');
const connectEnsureLogin = require('connect-ensure-login');

router.get('/:id',controller.getPlan);
router.post('/',connectEnsureLogin.ensureLoggedIn(),controller.postPlan);
router.put('/:id',connectEnsureLogin.ensureLoggedIn(),controller.putPlan);
router.delete('/:id',connectEnsureLogin.ensureLoggedIn(),controller.deletePlan);
router.post('/:id/buy',connectEnsureLogin.ensureLoggedIn(),controller.buyPlan);
router.get('/hasplan',connectEnsureLogin.ensureLoggedIn(),controller.hasPlan);

module.exports = router
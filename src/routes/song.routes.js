const express = require('express')
const router = express.Router()
const songController =   require('../controllers/song.controller');
const connectEnsureLogin = require('connect-ensure-login');

router.post('/', connectEnsureLogin.ensureLoggedIn(),songController.postSong);
router.put('/:id', connectEnsureLogin.ensureLoggedIn(),songController.putSong);
router.delete('/:id', connectEnsureLogin.ensureLoggedIn(),songController.deleteSong);
router.get('/:id',songController.getSong);
router.get('/:id/like', connectEnsureLogin.ensureLoggedIn(),songController.like);
router.post('/:id/report', connectEnsureLogin.ensureLoggedIn(),songController.report);
router.get('/:id/play', connectEnsureLogin.ensureLoggedIn(),songController.play);

module.exports = router
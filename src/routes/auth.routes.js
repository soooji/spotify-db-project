const express = require("express");
const router = express.Router();
const authController = require("../controllers/auth.controller");
const connectEnsureLogin = require('connect-ensure-login');

// router.get("/security-questions",connectEnsureLogin.ensureLoggedIn(), authController.getSecurityQuestions);
router.get("/security-questions", authController.getSecurityQuestions);
router.post("/login", authController.login);
router.post("/register", authController.register);
router.get("/logout", authController.logout);

router.post("/forgot-password/request", authController.getUserQuestion);
router.put("/forgot-password/change-password", authController.changePassword);
module.exports = router;


// // Retrieve all users
// router.get("/", authController.findAll);
// // Create a new uer
// router.post("/", authController.create);
// // Retrieve a single user with id
// router.get("/:id", authController.findById);
// // Update a user with id
// router.put("/:id", authController.update);
// // Delete a user with id
// router.delete("/:id", authController.delete);
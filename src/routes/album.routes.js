const express = require('express')
const router = express.Router()
const controller =   require('../controllers/album.controller');
const connectEnsureLogin = require('connect-ensure-login');

router.post('/', connectEnsureLogin.ensureLoggedIn(),controller.postAlbum);
router.put('/:id', connectEnsureLogin.ensureLoggedIn(),controller.putAlbum);
router.delete('/:id', connectEnsureLogin.ensureLoggedIn(),controller.deleteAlbum);
router.get('/:id',controller.getAlbum);
module.exports = router
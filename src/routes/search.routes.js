const express = require('express')
const router = express.Router()
const controller = require('../controllers/search.controller');
const connectEnsureLogin = require('connect-ensure-login');

router.post('/main', controller.main);
router.get('/user-playlists/:id', controller.getUserPlaylist);
router.get('/artist-works/:id', controller.getArtistWorks);
router.get('/user-followers/:id', controller.getFollowers);
router.get('/user-followings/:id', controller.getFollowings);
router.get('/user-followings-songs/',connectEnsureLogin.ensureLoggedIn(),controller.getLastSongsPlayedByFollowings);
router.get('/following-artists-songs/',connectEnsureLogin.ensureLoggedIn(),controller.getArtistsFiveSongs);


// router.post('/main', connectEnsureLogin.ensureLoggedIn(),controller.main);

module.exports = router
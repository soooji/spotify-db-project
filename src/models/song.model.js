"use strict";
var dbConn = require("./../../config/db.config");
const User = require("../models/user.model");
const Album = require("./album.model");
var mysql = require("mysql");

exports.get = async function (songId, result) {
  dbConn.query(
    `
        SELECT *
        From song
        WHERE id = ${mysql.escape(songId)}
      `,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        if (res.length == 0) {
          return result({ sqlMessage: "song not found!" }, null);
        } else {
          return result(null, res[0]);
        }
      }
    }
  );
};

exports.create = async function (userId, newSong, result) {
  let tempSong = { ...newSong };
  delete tempSong.album_id;
  let insertedSongId = null;
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "artist") {
        return result({ sqlMessage: "User is not artist!" }, null);
      } else {
        dbConn.query("INSERT INTO song set ?", tempSong, function (err, res) {
          if (err) {
            console.log("error: ", err);
            return result(err, null);
          } else {
            insertedSongId = res.insertId;
            dbConn.query(
              "INSERT INTO album_song set ?",
              {
                album_id: newSong.album_id,
                song_id: res.insertId,
                addition_date: new Date(),
              },
              function (err, res) {
                if (err) {
                  console.log("error: ", err);
                  return result(err, null);
                } else {
                  return result(null, insertedSongId);
                }
              }
            );
          }
        });
      }
    }
  });
};

exports.put = async function (userId, songId, song, result) {
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "artist") {
        return result({ sqlMessage: "User is not artist!" }, null);
      } else {
        dbConn.query(
          `SELECT publish.artist_id, publish.album_id, album_song.song_id
            FROM publish
            INNER JOIN album_song ON publish.album_id = album_song.album_id
            WHERE album_song.song_id = ?
            `,
          [songId],
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              if (res.length == 0) {
                return result({ sqlMessage: "Song not found!" }, null);
              }
              if (res[0].artist_id != userId) {
                return result(
                  { sqlMessage: "You're not the owner of this song!" },
                  null
                );
              }

              dbConn.query(
                "UPDATE song SET name=?,duration=? WHERE id = ?",
                [song.name, song.duration, songId],
                function (err, res) {
                  if (err) {
                    console.log("error: ", err);
                    return result(err, null);
                  } else {
                    return result(null, "Song updated successfully!");
                  }
                }
              );
            }
          }
        );
      }
    }
  });
};

exports.delete = async function (userId, songId, result) {
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "artist") {
        return result({ sqlMessage: "User is not artist!" }, null);
      } else {
        dbConn.query(
          `SELECT publish.artist_id, publish.album_id, album_song.song_id
            FROM publish
            INNER JOIN album_song ON publish.album_id = album_song.album_id
            WHERE album_song.song_id = ?
            `,
          [songId],
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              if (res.length == 0) {
                return result({ sqlMessage: "Song not found!" }, null);
              } else if (res[0].artist_id != userId) {
                return result(
                  { sqlMessage: "You're not the owner of this song!" },
                  null
                );
              } else {
                let albumId = res[0].album_id;
                dbConn.query(
                  "DELETE FROM song WHERE id = ?",
                  [songId],
                  function (err, res) {
                    if (err) {
                      console.log("error: ", err);
                      return result(err, null);
                    } else {
                      dbConn.query(
                        "DELETE FROM album_song WHERE song_id = ?",
                        [songId],
                        function (err, res) {
                          if (err) {
                            console.log("error: ", err);
                            return result(err, null);
                          } else {
                            dbConn.query(
                              `DELETE FROM song_like WHERE song_id = ${mysql.escape(
                                songId
                              )}`,
                              function (err, res) {
                                if (err) {
                                  console.log("error: ", err);
                                  result(null, err);
                                } else {
                                  dbConn.query(
                                    `SELECT * FROM album_song WHERE album_id = ${albumId}`,
                                    async function (err, res) {
                                      if (err) {
                                        console.log("error: ", err);
                                        return result(err, null);
                                      } else {
                                        if (res.length != 0) {
                                          return result(
                                            null,
                                            "Song deleted successfully!"
                                          );
                                        } else {
                                          await Album.delete(
                                            userId,
                                            albumId,
                                            function (err, res) {
                                              if (err) {
                                                console.log("error: ", err);
                                                return result(err, null);
                                              } else {
                                                return result(
                                                  null,
                                                  "Song and it's Album deleted successfully!"
                                                );
                                              }
                                            }
                                          );
                                        }
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  }
                );
              }
            }
          }
        );
      }
    }
  });
};

exports.like = async function (userId, songId, result) {
  dbConn.query(
    `SELECT * FROM song_like WHERE user_id = ${mysql.escape(
      userId
    )} and song_id = ${mysql.escape(songId)}`,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          dbConn.query(
            "INSERT INTO song_like set ?",
            {
              user_id: userId,
              song_id: songId,
            },
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                return result(null, "song liked!");
              }
            }
          );
        } else {
          dbConn.query(
            `DELETE FROM song_like WHERE user_id = ${mysql.escape(
              userId
            )} and song_id = ${mysql.escape(songId)}`,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                return result(null, "Song unliked!");
              }
            }
          );
        }
      }
    }
  );
};

exports.report = async function (userId,data, result) {
  let temp = {...data}
  temp.user_id = userId
  dbConn.query("INSERT INTO report set ?", temp, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      return result(null, "song reported!");
    }
  });
};

exports.play = async function (userId,songId, result) {
  let temp = {
    user_id:userId,
    song_id:songId,
    play_date: new Date()
  }
  dbConn.query("INSERT INTO play set ?", temp, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      return result(null, "song played!");
    }
  });
};

exports.todayPlays = async function (userId, result) {
  let today = new Date();
  dbConn.query(`
    SELECT *
    FROM play
    WHERE user_id = ${mysql.escape(userId)} and DATEDIFF(play_date, ${mysql.escape(today)}) = 0
  `, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      return result(null, res.length);
    }
  });
};

"use strict";
var dbConn = require("./../../config/db.config");
const User = require("../models/user.model");
var mysql = require("mysql");

exports.get = async function (id, result) {
  let resultObject = {};
  dbConn.query(
    `
        SELECT *
        From playlist
        WHERE id = ${mysql.escape(id)}
      `,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        if (res.length == 0) {
          return result({ sqlMessage: "playlist not found!" }, null);
        } else {
          resultObject.name = res[0].name;
          resultObject.last_update = res[0].last_update;
          resultObject.songs = [];

          dbConn.query(
            `  SELECT song.id, song.name, song.duration, playlist_song.addition_date
               FROM playlist_song
               INNER JOIN song ON playlist_song.song_id = song.id
               WHERE playlist_song.playlist_id = ${mysql.escape(id)}
            `,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                result(null, err);
              } else {
                if (res.length == 0) {
                  return result(null, resultObject);
                } else {
                  resultObject.songs = res;
                  return result(null, resultObject);
                }
              }
            }
          );
        }
      }
    }
  );
};

exports.create = async function (userId, newPlaylist, result) {
  let temp = { ...newPlaylist };
  temp.last_update = new Date();
  let insertedId = null;
  dbConn.query("INSERT INTO playlist set ?", temp, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      insertedId = res.insertId;
      dbConn.query(
        "INSERT INTO playlist_manage set ?",
        {
          user_id: userId,
          playlist_id: res.insertId,
        },
        function (err, res) {
          if (err) {
            console.log("error: ", err);
            return result(err, null);
          } else {
            return result(null, insertedId);
          }
        }
      );
    }
  });
};
exports.getUserOwnerPlaylist = async function (userId, result) {
  dbConn.query(
      `
        SELECT *
        From playlist_manage
        WHERE user_id = ${mysql.escape(userId)}
      `,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        return result(null, res.length);
      }
    }
  );
};
exports.like = async function (userId, playListId, result) {
  dbConn.query(
    `SELECT * FROM playlist_like WHERE user_id = ${mysql.escape(
      userId
    )} and playlist_id = ${mysql.escape(playListId)}`,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          dbConn.query(
            "INSERT INTO playlist_like set ?",
            {
              user_id: userId,
              playlist_id: playListId,
            },
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                return result(null, "playlist liked!");
              }
            }
          );
        } else {
          dbConn.query(
            `DELETE FROM playlist_like WHERE user_id = ${mysql.escape(
              userId
            )} and playlist_id = ${mysql.escape(playListId)}`,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                return result(null, "Playlist unliked!");
              }
            }
          );
        }
      }
    }
  );
};

exports.update = async function (userId, playListId, body, result) {
  dbConn.query(
    `SELECT * FROM playlist_manage where playlist_id = ${mysql.escape(
      playListId
    )} and user_id = ${mysql.escape(userId)}`,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          return result(
            {
              sqlMessage:
                "Playlist not found or you're not the owner of this Playlist",
            },
            null
          );
        } else {
          dbConn.query(
            `UPDATE playlist SET name = ${mysql.escape(
              body.name
            )},last_update = ${mysql.escape(
              new Date()
            )} WHERE id = ${mysql.escape(playListId)}`,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                return result(null, "Playlist updated!");
              }
            }
          );
        }
      }
    }
  );
};

exports.delete = async function (userId, playListId, result) {
  dbConn.query(
    `SELECT * FROM playlist_manage where playlist_id = ${mysql.escape(
      playListId
    )} and user_id = ${mysql.escape(userId)}`,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          return result(
            {
              sqlMessage:
                "Playlist not found or you're not the owner of this Playlist",
            },
            null
          );
        } else {
          dbConn.query(
            `DELETE FROM playlist WHERE id = ${mysql.escape(playListId)}`,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                dbConn.query(
                  `DELETE FROM playlist_song WHERE playlist_id = ${mysql.escape(
                    playListId
                  )}`,
                  function (err, res) {
                    if (err) {
                      console.log("error: ", err);
                      return result(err, null);
                    } else {
                      dbConn.query(
                        `DELETE FROM playlist_manage WHERE playlist_id = ${mysql.escape(
                          playListId
                        )}`,
                        function (err, res) {
                          if (err) {
                            console.log("error: ", err);
                            return result(err, null);
                          } else {
                            dbConn.query(
                              `DELETE FROM playlist_like WHERE playlist_id = ${mysql.escape(
                                playListId
                              )}`,
                              function (err, res) {
                                if (err) {
                                  console.log("error: ", err);
                                  return result(err, null);
                                } else {
                                  return result(null, "Playlist deleted!");
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  }
                );
              }
            }
          );
        }
      }
    }
  );
};

exports.addSong = async function (userId, songId,playListId, result) {
  dbConn.query(
    `SELECT * FROM playlist_manage where playlist_id = ${mysql.escape(
      playListId
    )} and user_id = ${mysql.escape(userId)}`,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          return result(
            {
              sqlMessage:
                "Playlist not found or you're not the owner of this Playlist",
            },
            null
          );
        } else {
          dbConn.query(
            "INSERT INTO playlist_song set ?",
            {
              song_id: songId,
              playlist_id: playListId,
              addition_date: new Date(),
            },
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                return result(null, res);
              }
            }
          );
        }
      }
    }
  );
};

exports.removeSong = async function (userId, songId, playListId, result) {
  dbConn.query(
    `SELECT * FROM playlist_manage where playlist_id = ${mysql.escape(
      playListId
    )} and user_id = ${mysql.escape(userId)}`,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          return result(
            {
              sqlMessage:
                "Playlist not found or you're not the owner of this Playlist",
            },
            null
          );
        } else {
          dbConn.query(
            `DELETE FROM playlist_song WHERE song_id = ${mysql.escape(
              songId
            )} and playlist_id = ${mysql.escape(playListId)}`,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                return result(null, res);
              }
            }
          );
        }
      }
    }
  );
};

exports.addManager = async function (userId, targetUserId, playListId, result) {
  dbConn.query(
    `SELECT * FROM playlist_manage where playlist_id = ${mysql.escape(
      playListId
    )} and user_id = ${mysql.escape(userId)}`,
    async function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          return result(
            {
              sqlMessage:
                "Playlist not found or you're not the owner of this Playlist",
            },
            null
          );
        } else {

         await User.getUserById(targetUserId,
            function (err, data) {
               if (err) {
                 console.log(err);
                 return result(err, null);
               } else {
                  if(!data) {
                     return result({sqlMessage: "User not found"}, null);
                  } else {
                     dbConn.query(
                     "INSERT INTO playlist_manage set ?",
                     {
                       user_id: targetUserId,
                       playlist_id: playListId,
                     },
                     function (err, res) {
                       if (err) {
                         console.log("error: ", err);
                         return result(err, null);
                       } else {
                         return result(null, "Target user is now manager");
                       }
                     }
                   );
                  }
               }
             }
            )
        }
      }
    }
  );
};
// exports.put = async function (userId, songId, song, result) {
//   User.getRole(userId, function (err, res) {
//     if (err) {
//       console.log("error: ", err);
//       return result(err, null);
//     } else {
//       if (res.userRole != "artist") {
//         return result({ sqlMessage: "User is not artist!" }, null);
//       } else {
//         dbConn.query(
//           `SELECT publish.artist_id, publish.album_id, album_song.song_id
//             FROM publish
//             INNER JOIN album_song ON publish.album_id = album_song.album_id
//             WHERE album_song.song_id = ?
//             `,
//           [songId],
//           function (err, res) {
//             if (err) {
//               console.log("error: ", err);
//               return result(err, null);
//             } else {
//               if (res.length == 0) {
//                 return result({ sqlMessage: "Song not found!" }, null);
//               }
//               if (res[0].artist_id != userId) {
//                 return result(
//                   { sqlMessage: "You're not the owner of this song!" },
//                   null
//                 );
//               }

//               dbConn.query(
//                 "UPDATE song SET name=?,duration=? WHERE id = ?",
//                 [song.name, song.duration, songId],
//                 function (err, res) {
//                   if (err) {
//                     console.log("error: ", err);
//                     return result(err, null);
//                   } else {
//                     return result(null, "Song updated successfully!");
//                   }
//                 }
//               );
//             }
//           }
//         );
//       }
//     }
//   });
// };

// exports.delete = async function (userId, playlist, result) {};

// module.exports = Song;

"use strict";
var dbConn = require("./../../config/db.config");
var utils = require("../../utils/main.utils");

//user object create

var User = function (user) {
  this.username = user.username;
  this.email = user.email;
  this.security_question = user.security_question;
  this.security_answer = user.security_answer;

  let hashResult = utils.saltHash(user.password);
  this.password = hashResult.passwordHash;
  this.salt = hashResult.salt;
};

User.getSecurityQuestions = function (result) {
  let securityQuestions = [
    {
      id: 1,
      title:
        "What was the house number and street name you lived in as a child?",
    },
    {
      id: 2,
      title:
        "What were the last four digits of your childhood telephone number?",
    },
    { id: 3, title: "What primary school did you attend?" },
  ];
  result(securityQuestions);
};

User.register = function (newUser, result) {
  dbConn.query("INSERT INTO users set ?", newUser, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res.insertId);
    }
  });
};

User.getUserQuestion = function (username, result) {
  dbConn.query("Select id, username, security_question from users where username = ? ", username, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

User.getUserSecurity = function (username, result) {
  dbConn.query("Select id, username, security_question, security_answer, salt  from users where username = ? ", username, function (err, res) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

User.changePassword = function (user, result) {
  dbConn.query(
    "UPDATE users SET password=? WHERE username = ?",
    [
      user.password,
      user.username
    ],
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        result(null, res);
      }
    }
  );
};

// User.findById = function (id, result) {
//   dbConn.query("Select id, username, email from users where id = ? ", id, function (err, res) {
//     if (err) {
//       console.log("error: ", err);
//       result(err, null);
//     } else {
//       result(null, res);
//     }
//   });
// };

// User.findAll = function (result) {
//   dbConn.query("Select id, username, email from users", function (err, res) {
//     if (err) {
//       console.log("error: ", err);
//       result(null, err);
//     } else {
//       console.log("user : ", res);
//       result(null, res);
//     }
//   });
// };

// User.update = function (id, user, result) {
//   dbConn.query(
//     "UPDATE users SET first_name=?,last_name=?,email=?,security_question=?,security_answer=? WHERE id = ?",
//     [
//       user.first_name,
//       user.last_name,
//       user.email,
//       user.security_question,
//       user.security_answer,
//       id,
//     ],
//     function (err, res) {
//       if (err) {
//         console.log("error: ", err);
//         result(null, err);
//       } else {
//         result(null, res);
//       }
//     }
//   );
// };

// User.delete = function (id, result) {
//   dbConn.query("DELETE FROM user WHERE id = ?", [id], function (err, res) {
//     if (err) {
//       console.log("error: ", err);
//       result(null, err);
//     } else {
//       result(null, res);
//     }
//   });
// };
module.exports = User;

"use strict";
var dbConn = require("./../../config/db.config");
var utils = require("../../utils/main.utils");
var mysql = require("mysql");
//user object create

var User = function (user) {
  this.username = user.username;
  this.email = user.email;
  this.security_question = user.security_question;
  this.security_answer = user.security_answer;

  let hashResult = utils.saltHash(user.password);
  this.password = hashResult.passwordHash;
  this.salt = hashResult.salt;
};

User.getUser = function (username, result) {
  let userProfile = {};
  let userId = 0;

  dbConn.query(
    "Select id, username, email from users where username = ? ",
    username,
    function (err, resu) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (resu.length != 0) {
          userId = resu[0].id;
          userProfile = { ...resu[0] };

          dbConn.query(
            `
            SELECT followers,followings
            FROM
            (SELECT users.id as user_id, count(follower_id) as followers
            FROM  follow
            JOIN  users ON follow.following_id = users.id
            WHERE users.id = ${mysql.escape(userId)}) f1
            JOIN  (SELECT users.id as user_id, count(following_id) as followings
                  FROM follow
                  JOIN users ON follow.follower_id = users.id
                  WHERE users.id = ${mysql.escape(userId)}) f2
                  ON f1.user_id = f1.user_id
               `,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                result(null, err);
              } else {
                userProfile.followers = res[0].followers;
                userProfile.followings = res[0].followings;
                dbConn.query(
                  "Select first_name, last_name, nationality, birth_date from client where user_id = ? ",
                  userId,
                  function (err, res) {
                    if (err) {
                      console.log("error: ", err);
                      return result(err, null);
                    } else {
                      if (res.length != 0) {
                        userProfile = {...userProfile, ...res[0] };
                        userProfile.role = "client";
                        return result(null, userProfile);
                      } else {
                        dbConn.query(
                          "Select artistic_name, nationality, start_date, is_active from artist where user_id = ? ",
                          userId,
                          function (err, res) {
                            if (err) {
                              console.log("error: ", err);
                              return result(err, null);
                            } else {
                              if (res.length != 0) {
                                userProfile = {...userProfile, ...res[0] };
                                userProfile.role = "artist";
                                return result(null, userProfile);
                              } else {
                                return result(null, userProfile);
                              }
                            }
                          }
                        );
                      }
                    }
                  }
                );
              }
            }
          );
        } else {
          return result({ sqlMessage: "User not found" }, null);
        }
      }
    }
  );
};

User.getUserById = function (userId, result) {
  dbConn.query("Select id from users where id = ? ", userId, function (
    err,
    res
  ) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.length != 0) {
        return result(null, res[0].id);
      } else {
        return result({ sqlMessage: "User not found" }, null);
      }
    }
  });
};

User.follow = function (userId, targetUserId, result) {
  User.getUserById(targetUserId, function (err, data) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (!data) {
        return result({ sqlMessage: "Target user not found!" }, null);
      } else {
        dbConn.query(
          `Select * from follow where follower_id = ${mysql.escape(
            userId
          )} and following_id = ${mysql.escape(targetUserId)}`,
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              if (res.length != 0) {
                dbConn.query(
                  `DELETE FROM follow WHERE follower_id = ${mysql.escape(
                    userId
                  )} and following_id = ${mysql.escape(targetUserId)}`,
                  function (err, res) {
                    if (err) {
                      console.log("error: ", err);
                      return result(err, null);
                    } else {
                      return result(null, "Target user unfollowed!");
                    }
                  }
                );
              } else {
                dbConn.query(
                  `INSERT INTO follow set ?`,
                  { follower_id: userId, following_id: targetUserId },
                  function (err, res) {
                    if (err) {
                      console.log("error: ", err);
                      return result(err, null);
                    } else {
                      return result(null, "User followd successfully!");
                    }
                  }
                );
              }
            }
          }
        );
      }
    }
  });
};

User.getUserId = function (username, result) {
  dbConn.query("Select id from users where username = ? ", username, function (
    err,
    res
  ) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.length != 0) {
        return result(null, res[0].id);
      } else {
        return result({ sqlMessage: "User not found" }, null);
      }
    }
  });
};

User.getRole = async function (userId, result) {
  let userRole = null;
  dbConn.query(
    "Select first_name, last_name, nationality, birth_date from client where user_id = ? ",
    userId,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        if (res.length != 0) {
          userRole = "client";
          return result(null, { userRole: userRole, userId: userId });
        } else {
          dbConn.query(
            "Select artistic_name, nationality, start_date, is_active from artist where user_id = ? ",
            userId,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                if (res.length != 0) {
                  userRole = "artist";
                  return result(null, {
                    userRole: userRole,
                    userId: userId,
                  });
                } else {
                  dbConn.query(
                    "Select * from admin where user_id = ? ",
                    userId,
                    function (err, res) {
                      if (err) {
                        console.log("error: ", err);
                        return result(err, null);
                      } else {
                        if (res.length != 0) {
                          userRole = "admin";
                          return result(null, {
                            userRole: userRole,
                            userId: userId,
                          });
                        } else {
                          return result(null, {
                            userRole: null,
                            userId: userId,
                          });
                        }
                      }
                    }
                  );
                }
              }
            }
          );
        }
      }
    }
  );
};

// Create user profile:
User.createClientProfile = function (username, profile, result) {
  let userId = null;
  dbConn.query(
    "Select id, username from users where username = ? ",
    username,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        userId = res[0].id;

        if (!userId) {
          return result({ sqlMessage: "User not found" }, null);
        }
        dbConn.query(
          "INSERT INTO client (user_id,first_name, last_name, nationality, birth_date) VALUES (?,?,?,?,?) ",
          [
            userId,
            profile.first_name,
            profile.last_name,
            profile.nationality,
            profile.birth_date,
          ],
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              return result(null, res);
            }
          }
        );
      }
    }
  );
};

User.createArtistProfile = function (username, profile, result) {
  let userId = null;
  dbConn.query(
    "Select id, username from users where username = ? ",
    username,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        userId = res[0].id;

        if (!userId) {
          return result({ sqlMessage: "User not found" }, null);
        }
        dbConn.query(
          "INSERT INTO artist (user_id,nationality, start_date, artistic_name, is_active) VALUES (?,?,?,?,?) ",
          [
            userId,
            profile.nationality,
            profile.start_date,
            profile.artistic_name,
            false, //default is active is false
          ],
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              return result(null, res);
            }
          }
        );
      }
    }
  );
};

// Update user profile:
User.update = function (userId, user, result) {
  User.getRole(userId, function (err, roleAndId) {
    if (err || !roleAndId.userRole) {
      return result({ sqlMessage: "User role not found" }, null);
    } else {
      let query = "";
      let data = [];
      if (roleAndId.userRole == "client") {
        query =
          "UPDATE client SET first_name=?,last_name=?,nationality=?,birth_date=? WHERE user_id = ?";
        data = [
          user.first_name,
          user.last_name,
          user.nationality,
          user.birth_date,
          roleAndId.userId,
        ];
      } else if (roleAndId.userRole == "artist") {
        query =
          "UPDATE artist SET nationality=?,start_date=?,artistic_name=? WHERE user_id = ?";
        data = [
          user.nationality,
          user.start_date,
          user.artistic_name,
          roleAndId.userId,
        ];
      } else {
        return result({ sqlMessage: "User not found" }, null);
      }
      dbConn.query(query, data, function (err, res) {
        if (err) {
          console.log("error: ", err);
          result(err, null);
        } else {
          console.log(res);
          result(null, res);
        }
      });
    }
  });
};

User.findByUsername = function (username, result) {
  dbConn.query(
    "Select id, username, email, password, salt from users where username = ? ",
    username,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

module.exports = User;
// User.findAll = function (result) {
//   dbConn.query("Select id, username, email from users", function (err, res) {
//     if (err) {
//       console.log("error: ", err);
//       result(null, err);
//     } else {
//       console.log("user : ", res);
//       result(null, res);
//     }
//   });
// };

// User.delete = function (id, result) {
//   dbConn.query("DELETE FROM user WHERE id = ?", [id], function (err, res) {
//     if (err) {
//       console.log("error: ", err);
//       result(null, err);
//     } else {
//       result(null, res);
//     }
//   });
// };

// User.create = function (newUser, result) {
//   dbConn.query("INSERT INTO users set ?", newUser, function (err, res) {
//     if (err) {
//       console.log("error: ", err);
//       result(err, null);
//     } else {
//       console.log(res.insertId);
//       result(null, res.insertId);
//     }
//   });
// };

// User.findById = function (id, result) {
//   dbConn.query("Select id, username, email from users where id = ? ", id, function (err, res) {
//     if (err) {
//       console.log("error: ", err);
//       result(err, null);
//     } else {
//       result(null, res);
//     }
//   });
// };

"use strict";
var dbConn = require("./../../config/db.config");
var utils = require("../../utils/main.utils");
const User = require("./user.model");
const Song = require("./song.model");
var mysql = require("mysql");

const Album = function () {};

Album.get = async function (albumId, result) {
  dbConn.query(
    `
      SELECT album.id AS album_id,
          album.name AS album_name,
          album.artist_id AS artist_id,
          album_song.addition_date AS song_addition_date,
          song.id AS song_id,
          song.name AS song_name,
          song.duration AS song_duration
      From ((album
        INNER JOIN album_song ON album.id = album_song.album_id)
        INNER JOIN song ON song.id = album_song.song_id)
      WHERE album_id = ${mysql.escape(albumId)}
    `,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        if (res.length == 0) {
          return result({ sqlMessage: "album not found!" }, null);
        } else {
          let albumSongs = [];
          for (let i in res) {
            albumSongs.push({
              id: res[i].song_id,
              name: res[i].song_name,
              duration: res[i].song_duration,
              addition_date: res[i].song_addition_date,
            });
          }
          return result(null, {
            name: res[0].album_name,
            artist_id: res[0].artist_id,
            songs: albumSongs,
          });
        }
      }
    }
  );
};

Album.create = async function (userId, newAlbum, result) {
  let tempAlbum = { ...newAlbum };
  if(newAlbum.songs.length == 1) {
    tempAlbum.name = newAlbum.songs[0].name
  }
  delete tempAlbum.songs;
  let insertedAlbumId = null;
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "artist") {
        return result({ sqlMessage: "User is not artist!" }, null);
      } else {
        dbConn.query(
          "INSERT INTO album set ?",
          { ...tempAlbum, artist_id: userId },
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              insertedAlbumId = res.insertId;
              dbConn.query(
                "INSERT INTO publish set ?",
                {
                  artist_id: userId,
                  album_id: res.insertId,
                  publish_date: new Date(),
                },
                async function (err, res) {
                  if (err) {
                    console.log("error: ", err);
                    return result(err, null);
                  } else {
                    for (let i in newAlbum.songs) {
                      try {
                        await Song.create(
                          userId,
                          { ...newAlbum.songs[i], album_id: insertedAlbumId },
                          function (err, res) {
                            if (err) {
                              console.log("error: ", err);
                              return result(err, null);
                            }
                          }
                        );
                      } catch (err) {
                        console.log(err);
                        return result({ sqlMessage: err }, null);
                      }
                    }
                    return result(null, "Album created successfully!");
                  }
                }
              );
            }
          }
        );
      }
    }
  });
};

Album.put = async function (userId, album_id, album, result) {
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "artist") {
        return result({ sqlMessage: "User is not artist!" }, null);
      } else {
        dbConn.query(
          `Select artist_id from publish where album_id = ${mysql.escape(
            album_id
          )}`,
          async function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              if (res.length == 0) {
                return result(
                  { sqlMessage: "Publish not found with this album id!" },
                  null
                );
              }
              if (res[0].artist_id != userId) {
                return result(
                  { sqlMessage: "You're not the owner of this album!" },
                  null
                );
              }

              dbConn.query(
                `UPDATE album SET name = ${mysql.escape(
                  album.name
                )},genre = ${mysql.escape(
                  album.genre
                )} WHERE id = ${mysql.escape(album_id)}`,
                async function (err, res) {
                  if (err) {
                    console.log("error: ", err);
                    return result(err, null);
                  } else {
                    return result(null, "Album updated successfully!");
                  }
                }
              );
            }
          }
        );
      }
    }
  });
};

Album.delete = async function (userId, album_id, result) {
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "artist") {
        return result({ sqlMessage: "User is not artist!" }, null);
      } else {
        dbConn.query(
          "DELETE FROM album WHERE id = " + mysql.escape(album_id),
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              dbConn.query(
                "DELETE FROM publish WHERE album_id = " +
                  mysql.escape(album_id),
                function (err, res) {
                  if (err) {
                    console.log("error: ", err);
                    return result(err, null);
                  } else {
                    console.log(`album_id: ${album_id}`);

                    dbConn.query(
                      "Select * from album_song where album_id = " +
                        mysql.escape(album_id),
                      function (err, albumSongs) {
                        if (err) {
                          console.log("error: ", err);
                          return result(err, null);
                        } else {
                          console.log("Album Songs:");
                          console.log(albumSongs); //TODO: Why is it empty?
                          console.log("\n");
                          if (albumSongs.length == 0) {
                            return result(null, "Album deleted successfully!");
                          } else {
                            for (let i in albumSongs) {
                              Song.delete(
                                userId,
                                albumSongs[i].song_id,
                                function (err, res) {
                                  if (err) {
                                    console.log("error: ", err);
                                    return result(err, null);
                                  }
                                }
                              );
                            }
                            return result(null, "Album deleted successfully!");
                          }
                        }
                      }
                    );
                  }
                }
              );
            }
          }
        );
      }
    }
  });
};

module.exports = Album;

"use strict";
var dbConn = require("./../../config/db.config");
const User = require("../models/user.model");
var mysql = require("mysql");
var moment = require('moment'); // require

exports.get = async function (planId, result) {
  dbConn.query(
    `
        SELECT *
        From plan
        WHERE id = ${mysql.escape(planId)}
      `,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        result(null, err);
      } else {
        if (res.length == 0) {
          return result({ sqlMessage: "Plan not found!" }, null);
        } else {
          return result(null, res[0]);
        }
      }
    }
  );
};


exports.create = async function (userId, newPlan, result) {
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "admin") {
        return result({ sqlMessage: "User is not admin!" }, null);
      } else {
        dbConn.query("INSERT INTO plan set ?", newPlan, function (err, res) {
          if (err) {
            console.log("error: ", err);
            return result(err, null);
          } else {
            return result(null, "Plan added successfully!");
          }
        });
      }
    }
  });
};

exports.put = async function (userId, planId, plan, result) {
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "admin") {
        return result({ sqlMessage: "User is not admin!" }, null);
      } else {
        dbConn.query(
          "UPDATE plan SET active_days = ? WHERE id = ?",
          [plan.active_days, planId],
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              return result(null, "Plan updated successfully!");
            }
          }
        );
      }
    }
  });
};

exports.delete = async function (userId, planId, result) {
  User.getRole(userId, function (err, res) {
    if (err) {
      console.log("error: ", err);
      return result(err, null);
    } else {
      if (res.userRole != "admin") {
        return result({ sqlMessage: "User is not admin!" }, null);
      } else {
        dbConn.query(
          `DELETE FROM plan WHERE id = ${mysql.escape(planId)}`,
          function (err, res) {
            if (err) {
              console.log("error: ", err);
              return result(err, null);
            } else {
              return result(null, "Plan deleted successfully!");
            }
          }
        );
      }
    }
  });
};

exports.buy = async function (userId, planId, planDetail, result) {
  let tempData = { ...planDetail };
  tempData.user_id = userId;
  tempData.plan_id = planId;
  let planActiveDays = 0;
  dbConn.query(
    `
        SELECT *
        From plan
        WHERE id = ${mysql.escape(planId)}
      `,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          return result({ sqlMessage: "Plan not found!" }, null);
        } else {
          planActiveDays = res[0].active_days;
          dbConn.query(
            `   
                SELECT *
                FROM buy                
                WHERE user_id = ${mysql.escape(userId)}
                ORDER BY end_date DESC
                LIMIT 1
            `,
            function (err, res) {
              if (err) {
                console.log("error: ", err);
                return result(err, null);
              } else {
                if (res.length != 0) {
                  let latestPlanDate = new Date(
                    `${res[0].end_date}`.replace(/-/g, "/")
                  );
                  let tempUserPlan = {
                    user_id: userId,
                    plan_id: planId,
                    card_number: planDetail.card_number,
                    card_expiration_date: planDetail.card_expiration_date,
                    buy_date: new Date(),
                    end_date: new Date(
                      latestPlanDate.getTime() + planActiveDays * 86400000
                    ), //86400000 = one day in miliseconds
                  };
                  dbConn.query(`INSERT INTO buy set ?`, tempUserPlan, function (
                    err,
                    res
                  ) {
                    if (err) {
                      console.log("error: ", err);
                      return result(err, null);
                    } else {
                      return result(null, "Plan bought successfully!");
                    }
                  });
                } else {
                  let tempUserPlan = {
                    user_id: userId,
                    plan_id: planId,
                    card_number: planDetail.card_number,
                    card_expiration_date: planDetail.card_expiration_date,
                    buy_date: new Date(),
                    end_date: new Date(Date.now() + planActiveDays * 86400000), //86400000 = one day in miliseconds
                  };
                  dbConn.query(`INSERT INTO buy set ?`, tempUserPlan, function (
                    err,
                    res
                  ) {
                    if (err) {
                      console.log("error: ", err);
                      return result(err, null);
                    } else {
                      return result(null, "Plan bought successfully!");
                    }
                  });
                }
              }
            }
          );
        }
      }
    }
  );
};

exports.hasPlan = async function (userId, result) {
  dbConn.query(
    `SELECT *
                FROM buy                
                WHERE user_id = ${mysql.escape(userId)}
                ORDER BY end_date DESC
                LIMIT 1`,
    function (err, res) {
      if (err) {
        console.log("error: ", err);
        return result(err, null);
      } else {
        if (res.length == 0) {
          return result(null, false);
        } else {
          if(moment(""+res[0].end_date).isAfter(new Date())) {
            return result(null, true);
          } else {
            return result(null, false);
          }
        }
      }
    }
  );
};

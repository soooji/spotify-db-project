"use strict";
const Album = require("../models/album.model");
var validate = require("validate.js");
var constraints = require("../validators/album.validator");
var songConstraints = require("../validators/song.validator");

exports.getAlbum = function (req,res) {
  Album.get(req.params.id, function (err, album) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage
            ? err.sqlMessage
            : "Can not get album!",
          details: err,
        },
      });
    } else {
      res.json(album);
    }
  });
}
exports.postAlbum = function (req, res) {
  let checkResult = validate(req.body, constraints.album);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Album.create(req.user.id, req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Entered data are not acceptable",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Album added successfully!",
          data: data,
        });
      }
    });
  }
};

exports.putAlbum = function (req, res) {
  let checkResult = validate(req.body, constraints.put);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Album.put(req.user.id, req.params.id, req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Entered data are not acceptable",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Album updated successfully!",
          data: data,
        });
      }
    });
  }
};

exports.deleteAlbum = function (req, res) {
  Album.delete(req.user.id, req.params.id, function (err, data) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage
            ? err.sqlMessage
            : "Entered data are not acceptable",
          details: err,
        },
      });
    } else {
      res.json({
        error: false,
        message: "Album and its Songs deleted successfully!",
        data: data,
      });
    }
  });
};

"use strict";
const Song = require("../models/song.model");
const Plan = require("../models/plan.model");
var validate = require("validate.js");
var constraints = require("../validators/song.validator");
const { invalid } = require("moment");

exports.getSong = function (req, res) {
  Song.get(req.params.id, function (err, data) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage ? err.sqlMessage : "Can not get song!",
          details: err,
        },
      });
    } else {
      res.json(data);
    }
  });
};

exports.postSong = function (req, res) {
  let checkResult = validate(req.body, constraints.song);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Song.create(req.user.id, req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Entered data are not acceptable",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Song added successfully!",
          data: data,
        });
      }
    });
  }
};

exports.putSong = function (req, res) {
  let checkResult = validate(req.body, constraints.song);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Song.put(req.user.id, req.params.id, req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Entered data are not acceptable",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Song updated successfully!",
          data: data,
        });
      }
    });
  }
};

exports.deleteSong = function (req, res) {
  Song.delete(req.user.id, req.params.id, function (err, data) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage
            ? err.sqlMessage
            : "Entered data are not acceptable",
          details: err,
        },
      });
    } else {
      res.json({
        error: false,
        message: "Song deleted successfully!",
        data: data,
      });
    }
  });
};

exports.like = function (req, res) {
  if (!req.params.id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Playlist not found",
        details: {},
      },
    });
  } else {
    Song.like(req.user.id, req.params.id, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not unlike/like Song!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.report = function (req, res) {
  let tempData = { ...req.body };
  tempData.song_id = req.params.id;
  let checkResult = validate(tempData, constraints.report);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Song.report(req.user.id, tempData, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Something went wrong!",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Song reported successfully!",
          data: data,
        });
      }
    });
  }
};

exports.play = function (req, res) {
  Plan.hasPlan(req.user.id, function (err, hasPlan) {
    if (!hasPlan) {
      Song.todayPlays(req.user.id, function (err, noOfPlays) {
        if (noOfPlays < 5) {
          Song.play(req.user.id, req.params.id, function (err, data) {
            if (err) {
              console.log(err);
              res.status(406).send({
                error: true,
                message: {
                  text: err.sqlMessage
                    ? err.sqlMessage
                    : "Something went wrong!",
                  details: err,
                },
              });
            } else {
              res.json({
                error: false,
                message: "Song played successfully!",
                data: data,
              });
            }
          });
        } else {
          res.status(406).send({
            error: true,
            message: {
              text: "Maximum plays reached (5 plays) - Buy plan first!",
              details: err,
            },
          });
        }
      });
    } else {
      Song.play(req.user.id, req.params.id, function (err, data) {
        if (err) {
          console.log(err);
          res.status(406).send({
            error: true,
            message: {
              text: err.sqlMessage ? err.sqlMessage : "Something went wrong!",
              details: err,
            },
          });
        } else {
          res.json({
            error: false,
            message: "Song played successfully!",
            data: data,
          });
        }
      });
    }
  });
};
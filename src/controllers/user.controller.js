"use strict";
const User = require("../models/user.model");
var validate = require("validate.js");
var constraints = require("../validators/user.validator");

exports.createClientProfile = function (req, res) {
  let checkResult = validate(req.body, constraints.clientProfile);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    User.createClientProfile(req.user.username, req.body, function (err, user) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Entered data are not acceptable",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Profile created successfully!",
          data: user,
        });
      }
    });
  }
};

exports.createArtistProfile = function (req, res) {
  let checkResult = validate(req.body, constraints.artistProfile);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    User.createArtistProfile(req.user.username, req.body, function (err, user) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Entered data are not acceptable",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Profile created successfully!",
          data: user,
        });
      }
    });
  }
};

exports.updateClientProfile = function (req, res) {
  let checkResult = validate(req.body, constraints.clientProfile);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    User.update(req.user.id, req.body, function (err, user) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Entered data are not acceptable",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Profile updated successfully!",
          data: user,
        });
      }
    });
  }
};

exports.updateArtistProfile = function (req, res) {
  let checkResult = validate(req.body, constraints.artistProfile);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    User.update(req.user.id, req.body, function (err, user) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Entered data are not acceptable",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Profile updated successfully!",
          data: user,
        });
      }
    });
  }
};

exports.getUser = function (req, res) {
  User.getUser(req.user.username, function (err, user) {
    if (err) res.send(err);
    res.json(user);
  });
};

exports.follow = function (req, res) {
  User.follow(req.user.id, req.params.id, function (err, data) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage
            ? err.sqlMessage
            : "Something went wrong whilte follow/ubfollow",
          details: err,
        },
      });
    } else {
      res.json({
        error: false,
        message: "Successfull Follow/Unfollow!",
        data: data,
      });
    }
  });
};

// exports.update = function (req, res) {
//   let checkResult = validate(req.body, constraints);
//   if (checkResult) {
//     res.status(406).send({
//       error: true,
//       message: {
//         text: "Entered data are not acceptable",
//         details: [...checkResult],
//       },
//     });
//   } else {
//     User.update(req.params.id, new User(req.body), function (err, user) {
//       if (err) res.send(err);
//       res.json({ error: false, message: "User successfully updated" });
//     });
//   }
// };

// exports.delete = function (req, res) {
//   User.delete(req.params.id, function (err, uesr) {
//     if (err) res.send(err);
//     res.json({ error: false, message: "User successfully deleted" });
//   });
// };

// exports.findAll = function (req, res) {
//   User.findAll(function (err, user) {
//     if (err) res.send(err);
//     res.send(user);
//   });
// };

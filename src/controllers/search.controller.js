"use strict";
const Search = require("../models/search.model");
var validate = require("validate.js");
var constraints = require("../validators/Search.validator");

exports.main = function (req, res) {
  let checkResult = validate(req.body, constraints.main);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Search.main(req.body.text, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not search!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.getUserPlaylist = function (req, res) {
  if (!req.params.id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Please enter user id!",
        details: {},
      },
    });
  } else {
    Search.getUserPlaylists(req.params.id, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not search!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.getArtistWorks = function (req, res) {
  if (!req.params.id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Please enter user id!",
        details: {},
      },
    });
  } else {
    Search.getArtistWorks(req.params.id, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not search!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.getFollowers = function (req, res) {
  if (!req.params.id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Please enter user id!",
        details: {},
      },
    });
  } else {
    Search.getFollowers(req.params.id, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not search!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.getFollowings = function (req, res) {
  if (!req.params.id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Please enter user id!",
        details: {},
      },
    });
  } else {
    Search.getFollowings(req.params.id, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not search!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.getLastSongsPlayedByFollowings = function (req, res) {
   if (!req.user) {
     res.status(406).send({
       error: true,
       message: {
         text: "Please Login first!",
         details: {},
       },
     });
   } else {
     Search.getLastSongsPlayedByFollowings(req.user.id, function (err, data) {
       if (err) {
         console.log(err);
         res.status(406).send({
           error: true,
           message: {
             text: err.sqlMessage ? err.sqlMessage : "Can not search!",
             details: err,
           },
         });
       } else {
         res.json(data);
       }
     });
   }
 };

 exports.getArtistsFiveSongs = function (req, res) {
  if (!req.user) {
    res.status(406).send({
      error: true,
      message: {
        text: "Please Login first!",
        details: {},
      },
    });
  } else {
    Search.getArtistsFiveSongs(req.user.id, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not search!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};
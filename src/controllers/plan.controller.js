"use strict";
const Plan = require("../models/plan.model");
var validate = require("validate.js");
var constraints = require("../validators/plan.validator");
var moment = require('moment'); // require

validate.extend(validate.validators.datetime, {
  // The value is guaranteed not to be null or undefined but otherwise it
  // could be anything.
  parse: function(value, options) {
    return +moment.utc(value);
  },
  // Input is a unix timestamp
  format: function(value, options) {
    var format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD hh:mm:ss";
    return moment.utc(value).format(format);
  }
});

exports.getPlan = function (req, res) {
  Plan.get(req.params.id, function (err, data) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage ? err.sqlMessage : "Can not get plan!",
          details: err,
        },
      });
    } else {
      res.json(data);
    }
  });
};

exports.postPlan = function (req, res) {
  let checkResult = validate(req.body, constraints.plan);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Plan.create(req.user.id, req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Something went wrong",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Plan added successfully!",
          data: data,
        });
      }
    });
  }
};

exports.putPlan = function (req, res) {
  let checkResult = validate(req.body, constraints.plan);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Plan.put(req.user.id, req.params.id, req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Something went wrong",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Plan updated successfully!",
          data: data,
        });
      }
    });
  }
};

exports.deletePlan = function (req, res) {
  Plan.delete(req.user.id, req.params.id, function (err, data) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage
            ? err.sqlMessage
            : "Something went wrong",
          details: err,
        },
      });
    } else {
      res.json({
        error: false,
        message: "Plan deleted successfully!",
        data: data,
      });
    }
  });
};

exports.buyPlan = function (req, res) {
  let checkResult = validate(req.body, constraints.buy);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Plan.buy(req.user.id, req.params.id, req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Something went wrong",
            details: err,
          },
        });
      } else {
        res.json({
          error: false,
          message: "Plan bought successfully!",
          data: data,
        });
      }
    });
  }
};

exports.hasPlan = function (req, res) {
  Plan.hasPlan(req.user.id, function (err, data) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage ? err.sqlMessage : "Can not check plan status!",
          details: err,
        },
      });
    } else {
      res.json(data);
    }
  });
};
"use strict";
const Playlist = require("../models/playlist.model");
const Song = require("../models/song.model");
const Plan = require("../models/plan.model");
var validate = require("validate.js");
var constraints = require("../validators/playlist.validator");

exports.getPlaylist = function (req, res) {
  if (!req.params.id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Playlist not found",
        details: {},
      },
    });
  } else {
    Playlist.get(req.params.id, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not find Playlist!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.like = function (req, res) {
  if (!req.params.id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Playlist not found",
        details: {},
      },
    });
  } else {
    Playlist.like(req.user.id, req.params.id, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Can not unlike/like Playlist!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.postPlaylist = function (req, res) {
  let checkResult = validate(req.body, constraints.playlist);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Plan.hasPlan(req.user.id, function (err, hasPlan) {
      if (!hasPlan) {
        Playlist.getUserOwnerPlaylist(req.user.id, function (err, noOfPlaylists) {
          if (noOfPlaylists < 5) {
            Playlist.create(req.user.id, req.body, function (err, data) {
              if (err) {
                console.log(err);
                res.status(406).send({
                  error: true,
                  message: {
                    text: err.sqlMessage ? err.sqlMessage : "Can not create playlist!",
                    details: err,
                  },
                });
              } else {
                res.json(data);
              }
            });
          } else {
            res.status(406).send({
              error: true,
              message: {
                text: "Maximum playlist create reached (5 plays) - Buy plan first!",
                details: err,
              },
            });
          }
        });
      } else {
        Playlist.create(req.user.id, req.body, function (err, data) {
          if (err) {
            console.log(err);
            res.status(406).send({
              error: true,
              message: {
                text: err.sqlMessage ? err.sqlMessage : "Can not create playlist!",
                details: err,
              },
            });
          } else {
            res.json(data);
          }
        });
      }
    });
  }
};

exports.putPlaylist = function (req, res) {
  let checkResult = validate(req.body, constraints.playlist);
  if (checkResult) {
    console.log(checkResult);
    res.status(406).send({
      error: true,
      message: {
        text: "Entered data are not acceptable",
        details: checkResult,
      },
    });
  } else {
    Playlist.update(req.user.id, req.params.id, req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage ? err.sqlMessage : "Can not update playlist!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.deletePlaylist = function (req, res) {
  Playlist.delete(req.user.id, req.params.id, function (err, data) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage ? err.sqlMessage : "Can not delete playlist!",
          details: err,
        },
      });
    } else {
      res.json(data);
    }
  });
};

exports.addManager = function (req, res) {
  Playlist.addManager(req.user.id, req.params.user_id, req.params.id, function (
    err,
    data
  ) {
    if (err) {
      console.log(err);
      res.status(406).send({
        error: true,
        message: {
          text: err.sqlMessage ? err.sqlMessage : "Can not add new manager!",
          details: err,
        },
      });
    } else {
      res.json(data);
    }
  });
};

exports.addSong = function (req, res) {
  if (!req.params.id || !req.params.song_id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Song or Playlist not found!",
        details: {},
      },
    });
  } else {
    Playlist.addSong(req.user.id, req.params.song_id, req.params.id, function (
      err,
      data
    ) {
      if (err) {
        console.log(err);
        res.status(406).send({
          error: true,
          message: {
            text: err.sqlMessage
              ? err.sqlMessage
              : "Can not add song to playlist!",
            details: err,
          },
        });
      } else {
        res.json(data);
      }
    });
  }
};

exports.removeSong = function (req, res) {
  if (!req.params.id || !req.params.song_id) {
    res.status(406).send({
      error: true,
      message: {
        text: "Song or Playlist not found!",
        details: {},
      },
    });
  } else {
    Playlist.removeSong(
      req.user.id,
      req.params.song_id,
      req.params.id,
      function (err, data) {
        if (err) {
          console.log(err);
          res.status(406).send({
            error: true,
            message: {
              text: err.sqlMessage
                ? err.sqlMessage
                : "Can not remove song from playlist!",
              details: err,
            },
          });
        } else {
          res.json(data);
        }
      }
    );
  }
};
// exports.postAlbum = function (req, res) {
//   let checkResult = validate(req.body, constraints.album);
//   if (checkResult) {
//     console.log(checkResult);
//     res.status(406).send({
//       error: true,
//       message: {
//         text: "Entered data are not acceptable",
//         details: checkResult,
//       },
//     });
//   } else {
//     Album.create(req.user.id, req.body, function (err, data) {
//       if (err) {
//         console.log(err);
//         res.status(406).send({
//           error: true,
//           message: {
//             text: err.sqlMessage
//               ? err.sqlMessage
//               : "Entered data are not acceptable",
//             details: err,
//           },
//         });
//       } else {
//         res.json({
//           error: false,
//           message: "Album added successfully!",
//           data: data,
//         });
//       }
//     });
//   }
// };

// exports.putAlbum = function (req, res) {
//   let checkResult = validate(req.body, constraints.put);
//   if (checkResult) {
//     console.log(checkResult);
//     res.status(406).send({
//       error: true,
//       message: {
//         text: "Entered data are not acceptable",
//         details: checkResult,
//       },
//     });
//   } else {
//     Album.put(req.user.id, req.params.id, req.body, function (err, data) {
//       if (err) {
//         console.log(err);
//         res.status(406).send({
//           error: true,
//           message: {
//             text: err.sqlMessage
//               ? err.sqlMessage
//               : "Entered data are not acceptable",
//             details: err,
//           },
//         });
//       } else {
//         res.json({
//           error: false,
//           message: "Album updated successfully!",
//           data: data,
//         });
//       }
//     });
//   }
// };

// exports.deleteAlbum = function (req, res) {
//   Album.delete(req.user.id, req.params.id, function (err, data) {
//     if (err) {
//       console.log(err);
//       res.status(406).send({
//         error: true,
//         message: {
//           text: err.sqlMessage
//             ? err.sqlMessage
//             : "Entered data are not acceptable",
//           details: err,
//         },
//       });
//     } else {
//       res.json({
//         error: false,
//         message: "Album and its Songs deleted successfully!",
//         data: data,
//       });
//     }
//   });
// };
